<?php
use PHPUnit\Framework\TestCase;
require('../src/Message.php');

class MessageTest extends TestCase {
    public function test_Replace_Message() {
        $message = new Message('abc');
        $this->assertEquals('abc', $message->Replace_Message('def'));
        $this->assertEquals('def', $message->Replace_Message('ghi'));
    }

    public function test_Get_Message() {
        $message = new Message('abc');
        $this->assertEquals('abc', $message->Get_Message());
        $message->Replace_Message('def');
        $this->assertEquals('def', $message->Get_Message());
        $message->Replace_Message(new MessageTest());
        $this->assertEquals('invalid message', $message->Get_Message());
    }
}
