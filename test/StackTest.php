<?php
use PHPUnit\Framework\TestCase;
require('../src/Stack.php');

class StackTest extends TestCase {
    public function test_Push() {
        $stack = new Stack(3);
        $this->assertEquals(true, $stack->Push(1));
        $this->assertEquals(true, $stack->Push(2));
        $this->assertEquals(true, $stack->Push(3));
        $this->assertEquals(false, $stack->Push(4));
        $this->assertEquals(false, $stack->Push(5));
        $this->assertEquals(false, $stack->Push(6));
    }

    public function test_Pop() {
        $stack = new Stack(10);
        $this->assertEquals(null, $stack->Pop());
        $stack->Push('a');
        $this->assertEquals('a', $stack->Pop());
        $stack->Push('b');
        $this->assertEquals('b', $stack->Pop());
        $stack->Push('c');
        $stack->Push('d');
        $this->assertEquals('d', $stack->Pop());
    }

    public function test_Count() {
        $stack = new Stack(5);
        $stack->Push(1);
        $stack->Push(2);
        $this->assertEquals(2, $stack->Count('integer'));
        $stack->Push('a');
        $this->assertEquals(3, $stack->Count('any'));
        $stack->Push(true);
        $stack->Push(false);
        $stack->Push(true);
        $this->assertEquals(1, $stack->Count('string'));
        $this->assertEquals(2, $stack->Count('boolean'));
        $this->assertEquals(5, $stack->Count('any'));
    }
}

