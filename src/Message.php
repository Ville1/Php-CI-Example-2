<?php

/**
 * Class Message
 * @author Ville Myllykangas
 */
class Message {
	private $message;

    /**
     * Message constructor.
     * @param string $message
     */
    public function __construct($message) {
        $this->Set_Message($message);
    }

    /**
     * @return string Stored message
     */
    public function Get_Message() {
        return $this->message;
    }

    /**
     * @param string $new_message New message that needs to be stored
     * @return string Old message
     */
	public function Replace_Message($new_message) {
		$old = $this->message;
		$this->Set_Message($new_message);
		return $old;
	}

    /**
     * Sets new message. If message is not string 'invalid message' is set instead.
     * @param string $message
     */
    private function Set_Message($message) {
        if(gettype($message) != 'string')
            $message = 'invalid message';
        $this->message = $message;
    }
}
