<?php

/**
 * Class Stack
 * @author Ville Myllykangas
 */
class Stack {
    private $max_size;
    private $stack;

    /**
     * Stack constructor.
     * @param int $max_size Maximum size of stack
     */
    public function __construct($max_size) {
        $this->max_size = $max_size;
        $this->stack = array();
    }

    /**
     * @param mixed $item Item that needs to be stored in stack
     * @return bool Did item get stored?
     */
    public function Push($item) {
        if(count($this->stack) == $this->max_size)
            return false;

        array_push($this->stack, $item);
        return true;
    }

    /**
     * @return mixed Item from stack
     */
    public function Pop() {
        return array_pop($this->stack);
    }

    /**
     * @param string $type Type of variable. If not specified or 'any' is given, will count all items
     * @return int Number
     */
    public function Count($type = 'any') {
        $count = 0;

        if($type == 'any')
            return count($this->stack);

        foreach ($this->stack as $item) {
            if(gettype($item) == $type) {
                $count++;
            }
        }
        return $count;
    }
}
